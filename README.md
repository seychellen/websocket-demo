# WebSocket-Demo

Beispiel-Projekt zur Verwendung von Websockets mit
- JAVA EE
- JavaScript
- TomEE

Hinweis: Im Branch `kotlin` findet man das gleiche Backend in Kotlin verfasst.

## Youtube
In dem folgenden Video wird erklärt, wie man das Projekt einrichtet und startet:

[WebSockets ganz einfach mit Pure-Jave und Pure-JavaScript](https://youtu.be/jw4v-Txjsqs) 